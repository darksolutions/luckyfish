package com.darkrpstudios.LuckyFish;

import com.darkrpstudios.LuckyFish.Managers.RewardsManager;
import com.darkrpstudios.LuckyFish.Utils.MessageHandler;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

public class LuckyFish extends JavaPlugin
{
    private static LuckyFish instance;

    private RewardsManager rewardManager;

    private PluginDescriptionFile pluginYML = getDescription();

    private File settingsFile;
    private File rewardsFile;

    private FileConfiguration settingsConfig;
    private FileConfiguration rewardsConfig;
  
    @Override
    public void onEnable()
    {
        instance = this;
        CreateYMLFiles();

        rewardManager = new RewardsManager(instance);
    }
    @Override
    public void onDisable()
    {

    }

    private void Runnable()
    {

    }

    private void registerEvents()
    {
        PluginManager pm = Bukkit.getPluginManager();

    }

    private void registerCommands()
    {

    }

    private void CreateYMLFiles()
    {
        settingsFile = new File(getDataFolder(), "settings.yml");
        rewardsFile = new File(getDataFolder(), "rewards.yml");

        if (!settingsFile.exists())
        {
            settingsFile.getParentFile().mkdirs();
            saveResource("settings.yml", false);
        }

        if (!rewardsFile.exists())
        {
            rewardsFile.getParentFile().mkdirs();
            saveResource("rewards.yml", false);
        }

        settingsConfig = YamlConfiguration.loadConfiguration(settingsFile);
        rewardsConfig = YamlConfiguration.loadConfiguration(rewardsFile);

    }

    public FileConfiguration getSettingsConfig()
    {
        return settingsConfig;
    }


    public void SaveSettingsConfig(FileConfiguration settingsNewConfig)
    {
        try
        {
            settingsNewConfig.save(settingsFile);
            settingsConfig = YamlConfiguration.loadConfiguration(settingsFile);
        }
        catch (IOException e)
        {
            MessageHandler.SendSpigotConsoleMessage("&4Failed To Save Updated Settings Config",  e.getStackTrace().toString());
        }
    }

    public FileConfiguration getRewardsConfig()
    {
        return rewardsConfig;
    }


    public void saveRewardsConfig(FileConfiguration rewardsNewConfig)
    {
        try
        {
            rewardsNewConfig.save(rewardsFile);
            rewardsConfig = YamlConfiguration.loadConfiguration(rewardsFile);
        }
        catch (IOException e)
        {
            MessageHandler.SendSpigotConsoleMessage("&4Failed To Save Updated Rewards Config",  e.getStackTrace().toString());
        }
    }


    public static LuckyFish GetInstance()
    {
        return instance;
    }

    public RewardsManager GetRewardManager()
    {
        return rewardManager;
    }

}

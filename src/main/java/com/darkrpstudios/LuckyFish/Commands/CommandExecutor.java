package com.darkrpstudios.LuckyFish.Commands;

import org.bukkit.command.CommandSender;

public abstract class CommandExecutor
{
    private String command, permission, usage;
    private boolean console, player;
    private int maxLength, minLength;

    public abstract void execute(CommandSender sender, String[] args);

    public void setBoth()
    {
        this.player = true;
        this.console = true;
    }

    public boolean isBoth()
    {
        return (this.player) && (this.console);
    }

    public String getCommand()
    {
        return this.command;
    }

    public void setCommand(String command)
    {
        this.command = command;
    }

    public String getPermission()
    {
        return this.permission;
    }

    public void setPermission(String permission)
    {
        this.permission = permission;
    }

    public boolean isConsole()
    {
        return this.console;
    }

    public void setConsole()
    {
        this.console = true;
        this.player = false;
    }

    public boolean isPlayer()
    {
        return this.player;
    }

    public void setPlayer()
    {
        this.player = true;
        this.console = false;
    }

    public int getMaxLength()
    {
        return this.maxLength;
    }

    public int getMinLength()
    {
        return this.minLength;
    }

       public void setMaxLength(int maxLength)
    {
        this.maxLength = maxLength;
    }

    public void setMinLength(int minLength)
    {
        this.minLength = minLength;
    }

    public String getUsage()
    {
        return this.usage;
    }

    public void setUsage(String usage)
    {
        this.usage = usage;
    }
}


package com.darkrpstudios.LuckyFish.Commands.SubCommands;

import com.darkrpstudios.LuckyFish.Commands.CommandExecutor;
import com.darkrpstudios.LuckyFish.LuckyFish;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class AddRewardCommand extends CommandExecutor
{
    private LuckyFish plugin;
    private FileConfiguration rewardsConfig;

    public AddRewardCommand(LuckyFish instance)
    {
        setCommand("addreward");
        setMaxLength(5);
        setMinLength(4);
        setUsage("/luckyfish addreward <item> <amount> <chance> [permission] [rare item]");
        setBoth();
        setPermission("luckyfish.admin.addreward");
        plugin = instance;
    }


    @Override
    public void execute(CommandSender sender, String[] args)
    {
        rewardsConfig = plugin.getRewardsConfig();

    }
}
package com.darkrpstudios.LuckyFish.Structs;

import org.bukkit.Material;

public class RewardsStructure
{

    private int limit, xpAmount;
    private double chance;
    private String itemName, permission, command;
    private Material itemMaterial;

    public RewardsStructure(int limit, int xpAmount, double chance, String itemName, String permission, String command, Material itemMaterial)
    {
        this.limit = limit;
        this.xpAmount = xpAmount;
        this.chance = chance;
        this.itemName = itemName;
        this.permission = permission;
        this.command = command;
        this.itemMaterial = itemMaterial;
    }

    public int getLimit() {
        return limit;
    }

    public int getXpAmount() {
        return xpAmount;
    }

    public double getChance() {
        return chance;
    }

    public String getItemName() {
        return itemName;
    }

    public String getPermission() {
        return permission;
    }

    public String getCommand() {
        return command;
    }

    public Material getItemMaterial() {
        return itemMaterial;
    }
}

package com.darkrpstudios.LuckyFish.Utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class MessageHandler
{
    public static void SendSpigotConsoleMessage(String msg, String stacktrace)
    {
        Bukkit.getConsoleSender().sendMessage( colorSpigot(msg) + "\n------------------------------" + colorSpigot(stacktrace) + "\n------------------------------");

    }

    public static String colorSpigot(String msg)
    {
        return ChatColor.translateAlternateColorCodes('&', msg);
    }
}

package com.darkrpstudios.LuckyFish.Managers;

import com.darkrpstudios.LuckyFish.LuckyFish;
import com.darkrpstudios.LuckyFish.Structs.RewardsStructure;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;

public class RewardsManager
{
    private LuckyFish plugin;


    private ArrayList<RewardsStructure> rewards = new ArrayList<RewardsStructure>();

    public RewardsManager(LuckyFish instance)
    {
        plugin = instance;
    }

    public ArrayList<RewardsStructure> getRewards()
    {
        return rewards;
    }

    public void PopulateRewards()
    {
        FileConfiguration rewardsConfig = plugin.getRewardsConfig();

        for(int i = 1; i < rewardsConfig.getConfigurationSection("Rewards").getKeys(false).size(); i++)
        {
            int xpAmount = rewardsConfig.getInt("Rewards." + i + ".Exp-Amount");
            int limit = rewardsConfig.getInt("Rewards." + i + ".Limit");

            double chance = rewardsConfig.getDouble("Rewards." + i + "Chance");

            String itemName = rewardsConfig.getString("Rewards." + i + "ItemName");
            String command = rewardsConfig.getString("Rewards." + i + "Command");
            String permission = rewardsConfig.getString("Rewards." + i + "Permission");

            Material itemMaterial = Material.getMaterial(rewardsConfig.getString("Rewards." + i + "ItemMaterial").toUpperCase());

            if(itemMaterial == null)
            {
                System.out.println(rewardsConfig.getString("Rewards." + i + "ItemMaterial") + " is not a valid minecraft material");
                continue;
            }

            rewards.add(new RewardsStructure(limit, xpAmount, chance, itemName, permission, command, itemMaterial));
        }
    }
}



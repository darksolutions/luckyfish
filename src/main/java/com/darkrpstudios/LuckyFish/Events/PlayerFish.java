package com.darkrpstudios.LuckyFish.Events;

import com.darkrpstudios.LuckyFish.LuckyFish;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerFishEvent;

public class PlayerFish implements Listener
{
    private LuckyFish plugin;
    private FileConfiguration settingsConfig, rewardsConfig;

    public PlayerFish(LuckyFish instance)
    {
        plugin = instance;
    }

    @EventHandler
    public void playerFish(PlayerFishEvent e)
    {
        Player player = e.getPlayer();

        settingsConfig = plugin.getSettingsConfig();
        rewardsConfig = plugin.getRewardsConfig();

        e.setCancelled(settingsConfig.getBoolean("Settings.Override-fishing"));
    }
}
